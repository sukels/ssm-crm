/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : ssm_crm

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 03/04/2022 11:28:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_dept
-- ----------------------------
DROP TABLE IF EXISTS `tbl_dept`;
CREATE TABLE `tbl_dept`  (
  `dept_id` int NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_dept
-- ----------------------------
INSERT INTO `tbl_dept` VALUES (1, '客户部');
INSERT INTO `tbl_dept` VALUES (2, '技术部');
INSERT INTO `tbl_dept` VALUES (3, '产品部');

-- ----------------------------
-- Table structure for tbl_emp
-- ----------------------------
DROP TABLE IF EXISTS `tbl_emp`;
CREATE TABLE `tbl_emp`  (
  `emp_id` int NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `d_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`emp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1013 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_emp
-- ----------------------------
INSERT INTO `tbl_emp` VALUES (1001, '张三', '男', 'zhangsan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1002, '李四', '男', 'lisi@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1003, '玛丽', '女', 'mali@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1004, '王五', '男', 'wangwu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1005, '赵六', '男', 'zhaoliu@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1006, '陈七', '女', 'chenqi@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1007, '李八', '女', 'liba@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1008, '赵九', '男', 'zhaojiu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1009, '阳光', '男', ' yangg@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1010, '二蛋', '男', 'erdan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1014, '张三', '男', 'zhangsan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1015, '李四', '男', 'lisi@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1016, '玛丽', '女', 'mali@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1017, '王五', '男', 'wangwu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1018, '赵六', '男', 'zhaoliu@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1019, '陈七', '女', 'chenqi@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1020, '李八', '女', 'liba@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1021, '赵九', '男', 'zhaojiu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1022, '阳光', '男', ' yangg@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1023, '二蛋', '男', 'erdan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1024, '张三', '男', 'zhangsan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1025, '李四', '男', 'lisi@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1026, '玛丽', '女', 'mali@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1027, '王五', '男', 'wangwu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1028, '赵六', '男', 'zhaoliu@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1029, '陈七', '女', 'chenqi@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1030, '李八', '女', 'liba@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1031, '赵九', '男', 'zhaojiu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1032, '阳光', '男', ' yangg@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1033, '二蛋', '男', 'erdan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1034, '张三', '男', 'zhangsan@qq.com', 3);
INSERT INTO `tbl_emp` VALUES (1035, '李四', '男', 'lisi@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1036, '玛丽', '女', 'mali@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1037, '王五', '男', 'wangwu@qq.com', 1);
INSERT INTO `tbl_emp` VALUES (1038, '赵六', '男', 'zhaoliu@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1039, '玛丽', '女', 'mali@qq.com', 2);
INSERT INTO `tbl_emp` VALUES (1040, '王五', '男', 'wangwu@qq.com', 1);

SET FOREIGN_KEY_CHECKS = 1;
