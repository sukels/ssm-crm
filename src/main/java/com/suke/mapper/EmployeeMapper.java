package com.suke.mapper;

import com.suke.pojo.Employee;
import com.suke.pojo.EmployeeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    long countByExample(EmployeeExample example);

    int deleteByExample(EmployeeExample example);

    int deleteByPrimaryKey(Integer empId);

    int insert(Employee record);

    int insertSelective(Employee record);

    List<Employee> selectByExample(EmployeeExample example);

    //新增显示员工列表信息的方法: 显示部门名称
    List<Employee> selectEmployeeExampleWithDept(EmployeeExample example);

    //新增一个根据编号查询具体的某个员工信息
    Employee selectByPrimaryKeyWithDept(Integer empId);

    Employee selectByPrimaryKey(Integer empId);

    int updateByExampleSelective(@Param("record") Employee record, @Param("example") EmployeeExample example);

    int updateByExample(@Param("record") Employee record, @Param("example") EmployeeExample example);

    int updateByPrimaryKeySelective(Employee record);

    int updateByPrimaryKey(Employee record);
}