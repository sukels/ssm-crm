package com.suke.service;

import com.suke.pojo.Employee;

import java.util.List;

/*
 * service层: 业务接口
 * */
public interface EmployeeService {

    //查询员工信息的列表：包含部门名称
    List<Employee> getAll();

    //员工保存的业务方法
    void saveEmp(Employee employee);

    //通过id获取员工信息
    Employee getEmp(Integer id);

    //真正更新保存员工信息的方法
    void updateEmp(Employee employee);

    //单个删除员工信息
    void deleteEmp(Integer id);

    //批量删除员工信息
    void deleteEmps(List<Integer> ids);
}
