package com.suke.service;

import com.suke.pojo.Department;

import java.util.List;

public interface DepartmentService {
    //查询部门信息
    List<Department> getDepts();
}
