package com.suke.service.impl;

import com.suke.mapper.DepartmentMapper;
import com.suke.pojo.Department;
import com.suke.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 * Service层: 业务接口的实现类
 * */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    /*注入da层对象*/
    @Autowired
    private DepartmentMapper departmentMapper;

    /*
     * 实现了查询部门信息方法
     * */
    @Override
    public List<Department> getDepts() {

        return departmentMapper.selectByExample(null);
    }
}
