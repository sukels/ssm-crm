package com.suke.service.impl;

import com.suke.mapper.EmployeeMapper;
import com.suke.pojo.Employee;
import com.suke.pojo.EmployeeExample;
import com.suke.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 * Service层: 业务接口的实现类
 * */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    /*注入dao层对象*/
    @Autowired
    private EmployeeMapper employeeMapper;

    /*
     * 实现了查询员工信息的业务方法: 显示部门信息
     * */
    @Override
    public List<Employee> getAll() {
        return employeeMapper.selectEmployeeExampleWithDept(null);
    }

    /*
     * 保存员工信息的方法：新增
     * */
    @Override
    public void saveEmp(Employee employee) {
        employeeMapper.insertSelective(employee);
    }

    /*
     * 实现通过id获取员工信息的业务方法
     * */
    @Override
    public Employee getEmp(Integer id) {
        return employeeMapper.selectByPrimaryKeyWithDept(id);
    }

    /*
     * 实现真正更新保存员工信息的方法
     * */
    @Override
    public void updateEmp(Employee employee) {

        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /*
     * 实现单个删除员工信息的方法
     * */
    @Override
    public void deleteEmp(Integer id) {

        employeeMapper.deleteByPrimaryKey(id);
    }

    /*
     * 实现批量删除员工信息
     * */
    @Override
    public void deleteEmps(List<Integer> ids) {

        EmployeeExample example = new EmployeeExample();
        EmployeeExample.Criteria criteria = example.createCriteria();
        criteria.andEmpIdIn(ids);
        employeeMapper.deleteByExample(example);
    }
}
