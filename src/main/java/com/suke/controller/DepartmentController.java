package com.suke.controller;

import com.suke.pojo.Department;
import com.suke.service.DepartmentService;
import com.suke.utils.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*
 * 查询部门信息的处理器
 * */
@Controller
public class DepartmentController {

    /*注入Service层对象*/
    @Autowired
    private DepartmentService departmentService;

    /*
     * 查询部门信息方法
     * */
    @RequestMapping("/depts")
    @ResponseBody
    public Msg getDepts() {

        List<Department> list = departmentService.getDepts();

        return Msg.success().add("depts", list);
    }
}