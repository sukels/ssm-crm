<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%--http://localhost:8080/11_ssm_crm/
http: request.getScheme(),localhost: request.getServerName(),
8080: request.getServerPort(),11_ssm_crm/src/main/webapp: request.getContextPath()--%>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + request.getContextPath();
%>

<html>
<head>
    <title>员工列表的页面</title>
    <%--引用jQuery的库文件--%>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.6.0.js"></script>
    <%--引入BootStrap框架--%>
    <link href="<%=basePath%>/static/bootstrap-3.4.1-dist/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="<%=basePath%>/static/bootstrap-3.4.1-dist/js/bootstrap.min.js"></script>
</head>
<body>

<%--点击新增按钮，显示新增员工信息的模态框--%>
<div class="modal fade" id="empAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <%--模态框头部--%>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">添加员工</h4>
            </div>
            <%--模态框内容--%>
            <div class="modal-body">
                <form class="form-horizontal">
                    <%--员工姓名--%>
                    <div class="form-group">
                        <label for="recipient-name" class="col-sm-2 control-label">姓名:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="recipient-name" name="empName">
                        </div>
                    </div>
                    <%--员工邮箱--%>
                    <div class="form-group">
                        <label for="recipient-email" class="col-sm-2 control-label">邮箱:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="recipient-email" name="email">
                        </div>
                    </div>

                    <%--员工的性别--%>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别:</label>
                        <div class="col-sm-10">
                            <label class="radio-inline">
                                <input type="radio" name="gender" id="gender1_add_input" value="男" checked="checked"/>男
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="gender" id="gender2_add_input" value="女"/>女
                            </label>
                        </div>
                    </div>

                    <%--部门信息的下拉选框--%>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">部门:</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="dId"></select>
                        </div>
                    </div>
                </form>
            </div>

            <%--保存和关闭的两个按钮--%>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="emp_sava_btn">保存</button>
            </div>
        </div>
    </div>
</div>

<%--编辑员工信息的模态框: 有部门信息--%>
<div class="modal fade" id="empUpdateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">员工修改</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">

                    <%--员工姓名--%>
                    <div class="form-group">
                        <label for="recipient-name" class="col-sm-2 control-label">姓名:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="empName_update_static"></p>
                        </div>
                    </div>

                    <%--员工邮箱--%>
                    <div class="form-group">
                        <label for="recipient-email" class="col-sm-2 control-label">邮箱:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_update_input" name="email">
                        </div>
                    </div>

                    <%--员工的性别--%>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别:</label>
                        <div class="col-sm-10">
                            <label class="radio-inline">
                                <input type="radio" name="gender" id="gender1_update_input" value="男"/>男
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="gender" id="gender2_update_input" value="女"/>女
                            </label>
                        </div>
                    </div>

                    <%--部门信息的下拉选框--%>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">部门：</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="dId"></select>
                        </div>
                    </div>
                </form>
            </div>

            <%--保存和关闭的两个按钮--%>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="emp_update_btn">保存</button>
            </div>
        </div>
    </div>
</div>

<%--员工列表展示的html页面--%>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>CRM员工 —— CRUD</h1>
        </div>
    </div>

    <br/><br/><br/>

    <%--两个按钮: 新增、删除（批量处理）--%>
    <div class="row">
        <div class="col-md-4 col-md-offset-8">
            <button class="btn btn-primary" id="emp_add_modal_btn">新增</button>
            <button class="btn btn-danger" id="emp_delete_all_btn">删除</button>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover" id="emps_table">
                <thead>
                <tr>
                    <th>
                        <input type="checkbox" id="check_all"/>
                    </th>
                    <th>编号</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>邮箱</th>
                    <th>部门</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <%--显示分页的信息--%>
    <div class="row">
        <%--分页的文字信息--%>
        <div class="col-md-6" id="page_info_area"></div>
        <%--分页条信息--%>
        <div class="col-md-6" id="page_nav_area"></div>
    </div>
</div>

<%--Ajax异步请求--%>
<script type="text/javascript">
    /*定义总条数和当前页*/
    let totalRecord, currentPage;
    /*页面在初次加载完成后，直接向ajax请求数据，获取到分页的数据，默认在第一页*/
    $(function () {
        //首页：第一页
        to_page(1);
    });

    /*显示员工信息*/
    function to_page(pn) {
        $("#check_all").prop("checked", false)
        $.ajax({
            url: "<%=basePath%>/emps",
            data: "pn=" + pn,
            type: "GET",
            success: function (result) {
                build_emps_table(result);
                build_page_info(result);
                build_page_nav(result);
            }
        });
    }

    /*显示员工信息的方法*/
    function build_emps_table(result) {
        //清空表格数据
        $("#emps_table tbody").empty();
        let emps = result.extend.pageInfo.list;
        $.each(emps, function (index, item) {
            let checkBoxTd = $("<td><input type='checkbox' class='check_item'/></td>");
            let empIdTd = $("<td></td>").append(item.empId);
            let empNameTd = $("<td></td>").append(item.empName);
            let genderTd = $("<td></td>").append(item.gender);
            let emailTd = $("<td></td>").append(item.email);
            let deptNameTd = $("<td></td>").append(item.department.deptName);

            //编辑的按钮
            let editBtn = $("<button></button>").addClass("btn btn-primary btn-sm edit_btn")
                .append($("<span></span>").addClass("glyphicon glyphicon-pencil")).append("编辑");

            /*为编辑按钮添加一个自定义的属性，来表示当前的员工id编号*/
            editBtn.attr("edit-id", item.empId);

            //删除的按钮
            let delBtn = $("<button></button>").addClass("btn btn-danger btn-sm delete_btn")
                .append($("<span></span>").addClass("glyphicon glyphicon-trash")).append("删除");

            /*为删除按钮添加一个自定义的属性，来表示当前的员工id编号*/
            delBtn.attr("del-id", item.empId);

            let btnTd = $("<td></td>").append(editBtn).append(" ").append(delBtn);

            $("<tr></tr>").append(checkBoxTd)
                .append(empIdTd)
                .append(empNameTd)
                .append(genderTd)
                .append(emailTd)
                .append(deptNameTd)
                .append(btnTd)
                .appendTo("#emps_table tbody");
        });
    }

    /*显示分页信息: 显示分页的数据*/
    function build_page_info(result) {
        //清空分页文字内容
        $("#page_info_area").empty();
        //添加分页文字内容
        $("#page_info_area").append("当前" + result.extend.pageInfo.pageNum + "页，共有" +
            result.extend.pageInfo.pages + "页，总记" +
            result.extend.pageInfo.total + "条记录数");
        totalRecord = result.extend.pageInfo.total;
        currentPage = result.extend.pageInfo.pageNum;
    }

    /*显示分页的分页条: 分页的实现*/
    function build_page_nav(result) {
        //清空分页条内容
        $("#page_nav_area").empty();
        let ul = $("<ul></ul>").addClass("pagination");

        /*构建li元素: 首页和上一页*/
        let firstPageLi = $("<li></li>").append($("<a></a>").append("首页").attr("href", "#"));
        let prePageLi = $("<li></li>").append($("<a></a>").append("&laquo;"));

        /*处理首页: 在第一页的时候失效：处理的是上一页*/
        if (result.extend.pageInfo.hasPreviousPage == false) {
            //隐藏第一页和上一页
            firstPageLi.addClass("disabled");
            prePageLi.addClass("disabled");
        } else {
            //给元素添加分页点击事件
            //第一页
            firstPageLi.click(function () {
                to_page(1);
            });
            //上一页
            prePageLi.click(function () {
                to_page(result.extend.pageInfo.pageNum - 1);
            });
        }

        /*构建li元素: 尾页和下一页*/
        let nextPageLi = $("<li></li>").append($("<a></a>").append("&raquo;"));
        let lastPageLi = $("<li></li>").append($("<a></a>").append("尾页").attr("href", "#"));

        /*处理尾页: 在最后一页的时候失效: 处理的是下一页*/
        if (result.extend.pageInfo.hasNextPage == false) {
            //隐藏下一页和最后一页
            nextPageLi.addClass("disabled");
            lastPageLi.addClass("disabled");
        } else {
            //给元素添加分页点击事件
            //下一页
            nextPageLi.click(function () {
                to_page(result.extend.pageInfo.pageNum + 1);
            });
            //最后一页
            lastPageLi.click(function () {
                to_page(result.extend.pageInfo.pages);
            });
        }

        /*添加首页和前一页的提示*/
        ul.append(firstPageLi).append(prePageLi);
        /*将1、2、3、4遍历给ul，给出页码提示(navigatepageNums: 所有导航页号)*/
        $.each(result.extend.pageInfo.navigatepageNums, function (index, item) {
            let numLi = $("<li></li>").append($("<a></a>").append(item));
            if (result.extend.pageInfo.pageNum == item) {
                numLi.addClass("active");
            }
            numLi.click(function () {
                to_page(item);
            });
            ul.append(numLi);
        });

        /*添加下一页和尾页的提示*/
        ul.append(nextPageLi).append(lastPageLi);
        //把ul添加到nav中
        let navEle = $("<nav></nav>").append(ul);
        navEle.appendTo("#page_nav_area");
    }

    /*点击新增按钮弹出模态框的方法*/
    $("#emp_add_modal_btn").click(function () {
        //清空表单中的数据(样式和内容)
        reset_form("#empAddModal form");
        //发送ajax请求，获取部门信息要显示在下拉列表中
        getDepts("#empAddModal select");
        //弹出模态框
        $("#empAddModal").modal({
            backdrop: "static"
        });
    });

    /*清空表单中的数据(样式和内容)*/
    function reset_form(ele) {
        $(ele)[0].reset();
        //清空表单中的样式
        $(ele).find("*").removeClass("has-error has-success");
        $(ele).find(".help-block").text("");
    }

    /*查询所有部门的信息显示在下拉列表中*/
    function getDepts(ele) {
        //清空数据
        $(ele).empty();
        $.ajax({
            url: "<%=basePath%>/depts",
            type: "GET",
            success: function (result) {
                $.each(result.extend.depts, function () {
                    let optionEle = $("<option></option>").append(this.deptName).attr("value", this.deptId);
                    optionEle.appendTo(ele);
                });
            }
        });
    }

    /*点击新增员工信息模态框中保存按钮，保存员工信息*/
    $("#emp_sava_btn").click(function () {
        //将模态框中的表单提交给服务器
        $.ajax({
            url: "<%=basePath%>/emp.action",
            type: "POST",
            data: $("#empAddModal form").serialize(),
            success: function (result) {
                if (result.code == 100) {
                    //保存数据成功，关闭模态框
                    $("#empAddModal").modal('hide');
                    /*添加数据成功以后回到最后一页*/
                    to_page(totalRecord);
                }
            }
        });
    });

    /*点击编辑按钮弹出编辑的模态框*/
    $(document).on("click", ".edit_btn", function () {
        //1、获取部门信息
        getDepts("#empUpdateModal select");
        //2、获取员工信息
        getEmp($(this).attr("edit-id"));
        //3、把员工的id传递给模态框中更新按钮
        $("#emp_update_btn").attr("edit-id", $(this).attr("edit-id"));
        //4、弹出模态框
        $("#empUpdateModal").modal({
            backdrop: "static"
        })
    });

    /*获取某个具体员工信息的方: 给编辑员工信息准备*/
    function getEmp(id) {
        $.ajax({
            url: "<%=basePath%>/emp/" + id,
            type: "GET",
            success: function (result) {
                let empData = result.extend.emp;
                $("#empName_update_static").text(empData.empName);
                $("#email_update_input").val(empData.email);
                $("#empUpdateModal input[name=gender]").val([empData.gender]);
                $("#empUpdateModal select").val([empData.dId]);
            }
        });
    }

    /*点击编辑的模态框中保存按钮: 保存用户信息*/
    $("#emp_update_btn").click(function () {
        //将模态框中的表单提交给服务器
        $.ajax({
            url: "<%=basePath%>/emp/" + $(this).attr("edit-id"),
            type: "PUT",
            data: $("#empUpdateModal form").serialize(),
            success: function (result) {
                if (result.code == 100) {
                    //保存数据成功，关闭模态框
                    $("#empUpdateModal").modal('hide');
                    /*添加数据成功以后回到最后一页*/
                    to_page(currentPage);
                }
            }
        });
    });

    /*单个删除员工信息*/
    $(document).on("click", ".delete_btn", function () {
        //设计弹出是否删除？
        let empName = $(this).parents("tr").find("td:eq(2)").text();
        let empId = $(this).attr("del-id");
        if (confirm("确定删除【" + empName + "】吗？")) {
            //确定删除
            $.ajax({
                url: "<%=basePath%>/emp/" + empId,
                type: "DELETE",
                success: function (result) {
                    alert(result.msg);
                    //删除回到当前页
                    to_page(currentPage);
                }
            });
        }
    });

    /*全部选和全部选*/
    $("#check_all").click(function () {
        $(".check_item").prop("checked", $(this).prop("checked"));

    })

    //判断全选是不是5条数据
    $(document).on("click", ".check_item", function () {
        let flag = $(".check_item:checked").length == $(".check_item").length;
        $("#check_all").prop("checked", flag);
    });

    /*实现真正批量删除的发的方法*/
    $("#emp_delete_all_btn").click(function () {
        let empNames = "";
        let del_idstr = "";
        $.each($(".check_item:checked"), function () {
            empNames += $(this).parents("tr").find("td:eq(2)").text() + ",";
            //获取批量的id值：组装成字符串
            del_idstr += $(this).parents("tr").find("td:eq(1)").text() + "-";
        });

        //去除姓名中多余的,
        empNames = empNames.substring(0, empNames.length - 1);
        //去除id中多余的-
        del_idstr = del_idstr.substring(0, del_idstr.length - 1);

        //点击批量删除的按钮弹出提示的弹框
        if (confirm("确定删除【" + del_idstr + "】吗？")) {
            //确认批量删除
            $.ajax({
                url: "<%=basePath%>/emp/" + del_idstr,
                type: "DELETE",
                success: function (result) {
                    alert(result.msg);
                    to_page(currentPage);
                }
            });
        }
    });
</script>
</body>
</html>